'''
@file lab3UI.py
@brief UI for file testing ADC Response on Nucleo
@details This file contains a script that measures the ADC Voltage response 
         to the User push button on the nucleo. The user is able to run the 
         script in Spyder here. The program will prompt the user to enter a
         start command "g", after which they will be prompted to push the
         user button on the nucleo. This UI communicates with a the
         \ref main.py file on the nucleo to compile the voltage response, 
         plot the response against time, and export the response to a .csv 
         file for further analysis.
@author Anil Singh
@date February 4, 2021
'''

import serial
import time
from matplotlib import pyplot
import csv

## Initialize serial port
ser = serial.Serial(port='COM6', baudrate = 115273,timeout=1)

print('''
      This program allows for the user to collect ADC voltage response data
      from the nucleo user push button. To initiate data collection, press
      g and hit enter.
      ''')
      
## User key input
user_input = input('Press g to initiate data collection: ')
print('Press User Button on Nucleo')

## Prevent invalid input
while True:
    if user_input != 'g':
        user_input = input('Invalid input. Press g to initiate data collection: ')
    else:
        break

## Write Data to Serial Port    
ser.write(str(user_input).encode('ascii'))
    

## Wait for Data from Serial Port
while True:
    
    if ser.in_waiting != 0:

        ## Convert ADC voltage data to list for analysis and plotting
        data = ser.readline().decode('ascii').split('  ')
        out = [m.strip("array('H', [])\n").split(', ') for m in data]
        data_output = [int(t)*(3.3/4095) for t in out[0]]
        time_stamp = [int(t) for t in out[1]]
        
        # Plot
        pyplot.plot(time_stamp, data_output)
        pyplot.xlabel('Time (ms)')
        pyplot.title('ADC Voltage Response to User Push Button')
        pyplot.ylabel('ADC Voltage Output (V)')
        
        # CSV File
        filename = 'ADC Voltage Response'+time.strftime('%Y%m%d-%H%M%S')+'.csv'
        with open(filename, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile)
            for n in range(len(time_stamp)):
                spamwriter.writerow([time_stamp[n], data_output[n]])
            
        # End Script
        break