'''
@file main.py
@brief file testing ADC Response on Nucleo
@details This file contains a script that measures the ADC Voltage response 
         to the User push button on the nucleo. The user must run this program
         from Spyder using the \ref lab3UI.py UI file. This program is housed
         on the nucleo, and serves to compile the voltage data and communicate
         accross the serial port with the UI.
@author Anil Singh
@date February 4, 2021
'''


from pyb import UART
import pyb
import array

pyb.enable_irq   # Enable interrupt

## ADC Calculation Variables
freq = 200000
ADC_range = 500
prebuffer_range = 20

## Initialize UART
uart = UART(2)

## Initialize Push Button
Button = pyb.Pin(pyb.Pin.board.PC0, mode=pyb.Pin.IN)

## Initialize ADC On Button Pin
adc = pyb.ADC(Button)

## ADC Pre-Buffer Array (Ensures correct push button reading)
prebuffer = array.array('H', (0 for index in range(prebuffer_range)))

## ADC Buffer
buffer = array.array('H', (0 for index in range(ADC_range)))

## Time Data Array for ADC
time_data = array.array('H', (int(i*(ADC_range/freq)*1e3) for i in range(ADC_range)))

## Timer Running at 200000 Hz
tim6 = pyb.Timer(6, freq = 200000)

## Boolean Flag indicating successful interrupt
flag = None
 
#Interrupt
def pushed(Line):
    global flag
    global user_input
    if user_input == 'g':
        flag = 1
        
## Initialize Interrupt on Falling Edge of push button
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback=pushed)
           
while True:
    if uart.any() != 0:
        user_input = uart.read().decode('ascii')
    elif flag == 1:
        flag = 0
        user_input = None
        break
  
while True:    
  #Check the ADC reading with prebuffer
  adc.read_timed(prebuffer, tim6) #timer 6 
  if prebuffer[-1] >= 10:
      break

#Read the ADC and send data over serial port
adc.read_timed(buffer, tim6) #timer 6
uart.write(str(buffer) + '  ' + str(time_data)+ '\n')

            
