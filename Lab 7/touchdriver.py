'''
@file touchdriver.py
@brief A driver to run ER-TFT080-1 Resistive Touch Panel
@details A driver to run a ER-TFT080-1 Resistive Touch Panel. The driver
contains methods to read the x-component, y-component, z-component, or all
3 components as a tuple.
@author Anil Singh
@author Kai Quizon
@author Rick Hall
@author Jacob Winkler
'''

import array
import pyb
import utime

class touchdriver:
    '''
    @brief A driver to run ER-TFT080-1 Resistive Touch Panel
    @details A driver to run a ER-TFT080-1 Resistive Touch Panel. The driver
             contains methods to read the x-component, y-component, 
             z-component, or all 3 components as a tuple.             
    '''
   
    def __init__(self,xmpin,ympin,xppin,yppin):
        '''
        @brief Initializes the touchdriver class pins
        @param xmpin xm pin of the touchscreen
        @param ympin ym pin of the touchscreen
        @param xppin xp pin of the touchscreen
        '''
        
        self.pos = array.array('f',[0, 0, 0])
        self.tch = False
                
        self.Cal = array.array('h',[300, 3820, 480, 3650])
        self.yCal = 107/(self.Cal[3]-self.Cal[2])
        self.xCal = 182/(self.Cal[1]-self.Cal[0])
        self.cCal = [2060,2065]        
        self.xm = pyb.Pin(xmpin)       
        self.yp = pyb.Pin(yppin)     
        self.xp = pyb.Pin(xppin)     
        self.ym = pyb.Pin(ympin)         
        self.zbuf = array.array('h',[0, 0, 0])        
        self.xbuf = array.array('h',[0, 0, 0])
        self.ybuf = array.array('h',[0, 0, 0])
            
        self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
        self.phigh.high()
        self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
        self.plow.low()            
        self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
        self.pread = pyb.ADC(self.xm)
        self.dset = 4
        
    def xreader(self):
        '''
        @brief Read the x location of the touch panel
        @details This method reads the touch panel's "x" output
        @return The x value of the touch location [mm].
        '''
        
        # Reset Pins for x-read
        if self.dset == 2 or self.dset == 4: # Y or Zy

            
            self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.yp)
            self.dset = 1
            
        elif self.dset == 3: # Zx


            self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 1            
        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.xbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[2] = self.pread.read()
        self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2])/3)

        self.pos[0] = round(self.xCal*(self.pos[0]-self.cCal[0]),2)
        
        return(int(self.pos[0]))
    
    def yreader(self):
        '''
        @brief Read the y location of the touch panel
        @details This method reads the touch panel's "x" output
        @return The y value of the touch location [mm].
        '''
        
        # Reset Pins for y-read
        if self.dset == 1 or self.dset == 3: # X or Zx

                
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.xm)
            self.dset = 2
            
        elif self.dset == 4: # Zy

            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.dset = 2

        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.ybuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[2] = self.pread.read()
        self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2])/3)
        
        self.pos[1] = round(self.yCal*(self.pos[1]-self.cCal[1]),2)
        
        return(int(self.pos[1]))
        
    def zreader(self):
        '''
        @brief Read the z location of the touch panel
        @details This method reads the touch panel's "z" output
        @return The z value of the touch location [mm].
        @return A boolean flag on whether the plate is being contacted or not.
        '''
        
         # Reset Pins for z-read
        if self.dset == 2:
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 4
            
        elif self.dset == 1:
                            
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            
            self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
            self.dset = 3

        
        # Read Sensor Data & filter        
        utime.sleep_us(5)
        self.zbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[2] = self.pread.read()
        
        self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2])/3)
        
        if 16 < self.pos[2] < 4080:
            self.tch = True
        else:
            self.tch = False
    
    def read(self):
        '''
        @brief Read the x,y, and z locations
        @details This method reads the touch panel's three outputs
        @return The x,y, and z values of the touch location [mm] as a tuple
        '''
        
        if self.tch == False:
            self.zreader()
            if self.tch == True:
                self.xreader()
                self.yreader()
        else:
            if self.dset == 1:
                self.xreader()
                self.zreader()
                self.yreader()
            else:
                self.yreader()
                self.zreader()
                self.xreader()
                
        return self.pos
    
            
if __name__ == '__main__':
    while True:
        try:
            test = input('\n'*20 + 'Indicate Test Direction:\n     - For X direction, enter "x"\n     -For Y direction, enter "y"\n     -For contact test, enter "z"\n     -For full-system test, enter "Full"\n\n                    >>>     ')
            if test != 'x' and test != 'y' and test != 'z' and test != 'Full':
                test = 'Full'

            
            tch = touchdriver('PA0','PA6','PA1','PA7')
            zflag = False
            
            if test == 'z':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        [zpos, zflag] = tch.zreader()
                        if zflag == True:
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n' * 20 + 'Contact Detected' + '\n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n' * 20 + 'No Contact Detected' + '\n'*7)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break     
                    
            elif test == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        xpos = tch.xreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' Contact at: X = ' + str(xpos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
                    
            elif test == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        ypos = tch.yreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' Contact at: Y = ' + str(ypos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  

            elif test == 'Full':
                while True:
                    try:
                        
                        if tch.tch == False:
                            t0 = utime.ticks_us()
                            tch.zreader()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                        if tch.tch == True:
                            t0 = utime.ticks_us()
                            position = tch.read()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n'*20)
                            print('\n'*2 +' Contact at: X = ' + str(position[0]) +'     Y = ' + str(position[1]) + '     Z = ' + str(position[2]) + ' \n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                            
                        else:
                            print('\n'*20)
                            # dsp.disp(tch)
                            print('\n' * 2 + 'No Contact Detected' + '\n'*2)
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
                    
        except KeyboardInterrupt:
            print('\n'*50 + 'Test Program Aborted \n\n')
            break 