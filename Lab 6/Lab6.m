%% State Space Simulation of Tilted Table
% ME 405 Winter 2021
%
% Anil Singh
%
% California Polytechnic State University, San Luis Obispo, CA
%
% 3/17/21

%% Constant Definitions            
r_m = 60E-3;           %m
l_r = 50E-3;           %m
r_b = 10.5E-3;         %m
r_g = 42E-3;           %m
l_p = 110E-3;          %m
r_p = 32.5E-3;         %m
r_c = 50E-3;           %m
m_b = 30E-3;           %kg
m_p = 400E-3;          %kg
I_p = 1.88E-3;         %kg m^2
b = 10E-3;             %N*m*(s/rad)
g = 9.810;             %m/s^2
I_b = (2/5)*m_b*r_b^2; %kg m^2
%% Variables

A = - (m_b*r_b^2 + m_b*r_c*r_b+I_b)/r_b;
C = -(m_b*r_b^2+I_b)/r_b;
D = -(m_b*r_b^3+m_b*r_c*r_b^2+I_b*r_b)/r_b;
H = (I_b*r_b+I_p*r_b+m_b*r_b^3+m_b*r_b*r_c^2+2*m_b*r_b^2*r_c+m_p*r_b*r_g^2)/r_b;
I = A*D+C*H;
J = -g*m_b*(r_b+r_c)^2-g*m_p*r_g-g*m_b*r_b;
K = -g*m_b*r_b;

%% State Space Model Matrices
A_mat = [   0          0        1    0;
            0          0        0    1;
       -g*m_b*D/I  (D*J+H*K)/I  0  D*b/I;
        g*m_b*C/I  (A*K-C*J)/I  0  -C*b/I]
     
B_mat = [0 0 (D/I)*(l_p/r_m) -(C/I)*(l_p/r_m)]'

C_mat = eye(4);

D_mat = zeros(4,1);

sys = ss(A_mat, B_mat, C_mat, D_mat, 'InputName','Tm', 'OutputName','x','StateName', {'x' 'theta' 'xdot' 'thetadot'})

%% Cointrolled State Space Model
K_feedback = [-0.3     -0.2    -0.05    -0.02];
  
out = sim('ControlledSys.slx',20);

%% Case 1
% Initially at rest directly above CG (no torque)
x0 = [0 0 0 0];
[y1,t1,x1] = initial(sys, x0, 1);

figure(1);
hold on
subplot(4,1,1)
plot(t1,x1(:,1))
xlabel('Time (s)')
ylabel('x (m)')
title('Linear Position of Ball')

subplot(4,1,2)
plot(t1, x1(:,2))
xlabel('Time (s)')
ylabel('theta (rad)')
title('Angular Tilt of Platform')

subplot(4,1,3)
plot(t1, x1(:,3))
xlabel('Time (s)')
ylabel('xdot (m/s)')
title('Linear Velocity of Ball')

subplot(4,1,4)
plot(t1, x1(:,4))
xlabel('Time (s)')
ylabel('thetadot (rad/s)')
title('Angular Velocity of Plate')

%% Case 2
% Initially at rest five centimeters off from the CV (no torque)
x0 = [0.05 0 0 0];
[y2,t2,x2] = initial(sys, x0, 0.4);

figure(2);
hold on
subplot(4,1,1)
plot(t2,x2(:,1))
xlabel('Time (s)')
ylabel('x (m)')
title('Linear Position of Ball')

subplot(4,1,2)
plot(t2, x2(:,2))
xlabel('Time (s)')
ylabel('theta (rad)')
title('Angular Tilt of Platform')

subplot(4,1,3)
plot(t2, x2(:,3))
xlabel('Time (s)')
ylabel('xdot (m/s)')
title('Linear Velocity of Ball')

subplot(4,1,4)
plot(t2, x2(:,4))
xlabel('Time (s)')
ylabel('thetadot (rad/s)')
title('Angular Velocity of Plate')

%% Case 3
% Initially at rest directly above CG, platform inclined by 5

x0 = [0 5*(pi/180) 0 0];
[y3,t3,x3] = initial(sys, x0, 0.4);

figure(3);
hold on
subplot(4,1,1)
plot(t3,x3(:,1))
xlabel('Time (s)')
ylabel('x (m)')
title('Linear Position of Ball')

subplot(4,1,2)
plot(t3, x3(:,2))
xlabel('Time (s)')
ylabel('theta (rad)')
title('Angular Tilt of Platform')

subplot(4,1,3)
plot(t3, x3(:,3))
xlabel('Time (s)')
ylabel('xdot (m/s)')
title('Linear Velocity of Ball')

subplot(4,1,4)
plot(t3, x3(:,4))
xlabel('Time (s)')
ylabel('thetadot (rad/s)')
title('Angular Velocity of Plate')

%% Case 4
% Initially at rest directly above CG, impulse of 1 mNm*s

x0 = [0 0 0 0];
[y4t,t4t,x4t] = impulse(sys, 0.4);
t4 = t4t;
u = zeros(size(t2));
u(1:10) = 1E-3;
[y4,t4,x4] = lsim(sys,u,t4,x0);

figure(4);
hold on
subplot(4,1,1)
plot(t4,x4(:,1))
xlabel('Time (s)')
ylabel('x (m)')
title('Linear Position of Ball')

subplot(4,1,2)
plot(t4, x4(:,2))
xlabel('Time (s)')
ylabel('theta (rad)')
title('Angular Tilt of Platform')

subplot(4,1,3)
plot(t4, x4(:,3))
xlabel('Time (s)')
ylabel('xdot (m/s)')
title('Linear Velocity of Ball')

subplot(4,1,4)
plot(t4, x4(:,4))
xlabel('Time (s)')
ylabel('thetadot (rad/s)')
title('Angular Velocity of Plate')


%% Controlled System
% Initially at rest five centimeters off from the CV (no torque
% With Controlled System Feedback

figure(5);
hold on
subplot(4,1,1)
plot(out.tout,out.simout(:,1))
xlabel('Time (s)')
ylabel('x (m)')
title('Linear Position of Ball')

subplot(4,1,2)
plot(out.tout, out.simout(:,2))
xlabel('Time (s)')
ylabel('theta (rad)')
title('Angular Tilt of Platform')

subplot(4,1,3)
plot(out.tout, out.simout(:,3))
xlabel('Time (s)')
ylabel('xdot (m/s)')
title('Linear Velocity of Ball')

subplot(4,1,4)
plot(out.tout, out.simout(:,4))
xlabel('Time (s)')
ylabel('thetadot (rad/s)')
title('Angular Velocity of Plate')