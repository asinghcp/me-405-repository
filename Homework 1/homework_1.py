'''
@file homework_1.py
@brief file defining the function getChange for homework assignment 1
@details This file contains a function called getChange. getChange is designed
         to accept a payment and store the price for an item. When a payment
         is given, a tuple representing the change is returned. 
@author Anil Singh
@date January 13, 2021
'''
def getChange(price, payment):
    '''
    @brief getChange function for homework assignment 1.
    @details This contains a function called getChange. getChange is designed
             to accept a payment and store the price for an item. When a
             payment is given, a tuple representing the change is returned. 
    @param price This is the price of the desired object. Price is an integer in cents.
    @param payment This is the payment, which indicates the given payment denominations as a tuple. 
    @return The required change is returned in the fewest number of denominations.
    '''
    ## Convert payment into pennies and references given tuple
    pennies  = 1*payment[0]
    nickels  = 5*payment[1]
    dimes    = 10*payment[2]
    quarters = 25*payment[3]
    ones     = 100*payment[4]
    fives    = 500*payment[5]
    tens     = 1000*payment[6]
    twenties = 2000*payment[7]
    
    ## Stores total payment value in pennies
    pay_amount = pennies + nickels + dimes + quarters + ones + fives + tens + twenties
    
    ## Inform user they have insufficient funds (Returns "None")
    change = pay_amount - price
    if change < 0:
        return None
    
    ## Compute change
    elif change >= 0:
        value = [2000, 1000, 500, 100, 25, 10, 5, 1]
        
        ## Create empty list for storing list of tuple values
        change_list = []      
        for n in value:
            ## Algebra for connversions and remainders
            if change - n >= 0:
                x = change - change%n
                y = x/n
                change_list.append(int(y))
                change = change - x
            elif change - n < 0:
                change_list.append(0)
            else:
                pass
            
        ## Switch order of list
        change_list.reverse()

        ## Convert list to tuple 
        return tuple(change_list)
    
if __name__ == "__main__":
    print('Computing Change:')
    
    ## Edit this to change the price of the item in pennies
    item_price = 0
    item_payment = (0, 0, 0, 0, 0, 0, 0, 1)
    item_change = getChange(item_price, item_payment)
    print(item_change)

    