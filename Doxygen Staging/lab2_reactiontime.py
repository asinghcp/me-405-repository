'''
@file lab2_reactiontime.py
@brief file running a reaction timer on the nucleo
@details This file contains a script that responds to an LED lighting. The
         user is able to use a push button to measure their reaction time.
         the program will record the reaction time taken to respond by the 
         user (in milliseconds).
@author Anil Singh
@date January 27, 2021
'''

import pyb
import utime
import random as rand
pyb.enable_irq

def pushed(line):
    '''
    @brief This function is called when the push button is pressed. It records the corresponding time.
    @param line Allows for interrupt code to funtion through multiple loops.
    '''
    # Interrupt Function
    if reaction_time[i] == 0:               
        reaction_time[i] = tim2.counter()   # Stores reaction time
        
        
    else:
        print('Please wait for LED.')
        
#Declare Global Variables
global i

# Initialize timers and pins
pinA5   = pyb.Pin (pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)     #LED
pinPC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)      #Push Button
tim2    = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF) #Timer 2
tim5    = pyb.Timer(5, prescaler = 79, period = 0x7FFFFFFF) #Timer 5

# Initialize utime
start_time = utime.ticks_us()
        
#Preallocate time lists
reaction_time = [0]
i=0    
    
#Initialize Interrupt
extint = pyb.ExtInt(pinPC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback=pushed)    

while True:
    try:        
        if len(reaction_time) > i:
            var = rand.randint(1,1000)  #Generate random integer
            delay = 2 + var / 1000      #Apply random integer to delay
            tim5.counter(0)             #Prevent premature delay trigger
            utime.sleep(delay)          #Pause for delay duration
            pinA5.high()                #Turn on LED
            tim2.counter(0)             #Timer starts counting
            utime.sleep(1)              #Force runtime of 1s
            pinA5.low()                 #Turn off LED
            i=len(reaction_time)
            
        elif reaction_time[len(reaction_time) - 1] != 0:
            reaction_time.append(0)
            
    except KeyboardInterrupt:
        print(reaction_time)
        avresponse = sum(reaction_time)/(len(reaction_time) - 1)
        print('Average response time: ' + str(avresponse) + ' microseconds.')
        quit()