/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "References", "index.html#sec_references", null ],
    [ "Directory", "index.html#sec_directory", null ],
    [ "Vendotron (Lab 1)", "page_vendotron.html", [
      [ "Description", "page_vendotron.html#page_vendotron_desc", null ],
      [ "State Transition Diagram", "page_vendotron.html#page_vendotron_sys", null ],
      [ "Source Code Access", "page_vendotron.html#page_vendotron_src", null ],
      [ "Documentation", "page_vendotron.html#page_vendotron_doc", null ]
    ] ],
    [ "Reaction Timer (Lab 2)", "page_reaction_time.html", [
      [ "Description", "page_reaction_time.html#page_reaction_time_desc", null ],
      [ "Source Code Access", "page_reaction_time.html#page_reaction_time_src", null ],
      [ "Documentation", "page_reaction_time.html#page_reaction_time_doc", null ]
    ] ],
    [ "ADC Voltage Response to User Button (Lab 3)", "page_ADC.html", [
      [ "Description", "page_ADC.html#page_ADC_desc", null ],
      [ "Source Code Access", "page_ADC.html#page_ADC_src", null ],
      [ "Documentation", "page_ADC.html#page_ADC_doc", null ]
    ] ],
    [ "MCP9808 Temperature Measurement (Lab 4)", "page_temp.html", [
      [ "Description", "page_temp.html#page_temp_desc", null ],
      [ "Source Code Access", "page_temp.html#page_temp_src", null ],
      [ "Documentation", "page_temp.html#page_temp_doc", null ]
    ] ],
    [ "Feeling Tipsy? (Lab 5)", "page_Feeling_Tipsy.html", [
      [ "Description", "page_Feeling_Tipsy.html#page_Feeling_Tipsy_desc", null ]
    ] ],
    [ "Simulation or Reality? (Lab 6)", "page_simulation_or_reality.html", [
      [ "Description", "page_simulation_or_reality.html#page_simulation_or_reality_desc", null ],
      [ "Hand Calculations: Linearization", "page_simulation_or_reality.html#page_simulation_or_reality_hc", null ],
      [ "MATLAB Outputs", "page_simulation_or_reality.html#page_simulation_or_reality_MATLAB", null ],
      [ "Source Code Access", "page_simulation_or_reality.html#page_simulation_or_reality_src", null ]
    ] ],
    [ "Feeling Touchy? (Lab 7)", "page_Feeling_Touchy.html", [
      [ "Description", "page_Feeling_Touchy.html#page_Feeling_Touchy_desc", null ],
      [ "Source Code Access", "page_Feeling_Touchy.html#page_Feeling_Touchy_src", null ],
      [ "Documentation", "page_Feeling_Touchy.html#page_Feeling_Touchy_doc", null ]
    ] ],
    [ "Term Project Part I (Lab 8 & Term Project)", "page_termI.html", [
      [ "Description", "page_termI.html#page_termI_desc", null ],
      [ "Detailed Documentation", "page_termI.html#page_termI_link", null ],
      [ "Source Code Access", "page_termI.html#page_termI_src", null ],
      [ "Documentation", "page_termI.html#page_termI_doc", null ]
    ] ],
    [ "Term Project Hardware (Term Project)", "page_TPH.html", [
      [ "Description", "page_TPH.html#page_TPH_Desc", null ],
      [ "Controller Mounts", "page_TPH.html#page_TPH_Controller", null ],
      [ "Actuators", "page_TPH.html#page_TPH_Motor", null ],
      [ "Sensor 1: Output Shaft Encoder", "page_TPH.html#page_TPH_Encoder", null ],
      [ "Sensor 2: BNO055 IMU", "page_TPH.html#page_TPH_IMU", null ],
      [ "Sensor 3: Touch Panel", "page_TPH.html#page_TPH_touch", null ],
      [ "Hardware Functionality", "page_TPH.html#page_TPH_function", null ]
    ] ],
    [ "Term Project Software (Term Project)", "page_TPS.html", [
      [ "Description", "page_TPS.html#page_TPS_desc", null ],
      [ "Source Code Access", "page_TPS.html#page_TPS_src", null ],
      [ "Drivers", "page_TPS.html#page_TPS_drivers", null ],
      [ "Task Queueing and Priority Organization", "page_TPS.html#page_TPS_fileshare", null ],
      [ "Fault Handling", "page_TPS.html#page_TPS_Fault", null ],
      [ "General Functionality", "page_TPS.html#page_TPS_Functionality", null ],
      [ "User Manual", "page_TPS.html#page_TPS_User", null ]
    ] ],
    [ "Term Project Results (Term Project)", "page_TPR.html", [
      [ "Description", "page_TPR.html#page_TPR_desc", null ],
      [ "Tuning the Table", "page_TPR.html#page_TPR_tuning", null ],
      [ "Balancing the Ball", "page_TPR.html#page_TPR_balancing", null ],
      [ "Partnership", "page_TPR.html#page_TPR_partner", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"lab4nucleo_8py.html#abdae185c5926423ba7c1a774f07e28dc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';