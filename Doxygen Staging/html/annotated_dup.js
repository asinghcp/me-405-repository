var annotated_dup =
[
    [ "bno055", null, [
      [ "BNO055", "classbno055_1_1BNO055.html", "classbno055_1_1BNO055" ]
    ] ],
    [ "bno055_base", null, [
      [ "BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", "classbno055__base_1_1BNO055__BASE" ]
    ] ],
    [ "busy_task", null, [
      [ "BusyTask", "classbusy__task_1_1BusyTask.html", "classbusy__task_1_1BusyTask" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "MCP9808", null, [
      [ "MCP9808", "classMCP9808_1_1MCP9808.html", "classMCP9808_1_1MCP9808" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchdriver", null, [
      [ "touchdriver", "classtouchdriver_1_1touchdriver.html", "classtouchdriver_1_1touchdriver" ]
    ] ]
];