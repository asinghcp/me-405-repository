"""
@file main_405_L4.py

@package 405_Lab0x04

@brief main script for \ref 405_Lab0x04 assignment. Runs temperature recorder.

@details This code combines a lot of concepts from previous labs in ME 305 and
         ME 405! This is a main code file that references the MCP9808 class
         and demonstrates its ability to record data accurately. The main file will
         only record data if the i2c device is connected properly and is the
         Adafruit MCP9808 temperature sensor. When the user button is pressed on
         the Nucleo, the temperature of the Nucleo core will be recorded along with
         the ambient temperature from the MCP9808 for 8 hours and stored in a csv file
         on the Nucleo!

         \n\nSource Code: https://bitbucket.org/asinghcp/lab-4-shared/src/master/main_405_L4.py

         \nExample csv file: https://bitbucket.org/asinghcp/lab-4-shared/src/master/Temperatures.csv

         \n\n Sample Data plot (generated with Spyder):
         \image html "405_lab4_dataplot.png"

@author Benjamin Presley

@date February 11, 2021

@copyright Available for use with consent of Ben Presley.
"""

import pyb
import micropython
from MCP9808 import MCP9808

##---------------------------System Variables-------------------------------##
recording = False # set global recording variable to False
init_time = 0 # predefine time_init
last_time = 0 # predefine last_time
rec_resolution = 15*1000 # [milliseconds], defines the recording period

# Define Data filename
filename = 'Temperatures.csv'


##-------------------Initialize the Core Temperature Sensor-----------------##

# Initialize ADCAll Object -- this measures properties of core
adc = pyb.ADCAll(12, 0x70000)

#Run a vref check to fix strange error
adc.read_vref()


##----------------------------Button function-------------------------------##

# Allocate external interrupt memory
micropython.alloc_emergency_exception_buf(1000)

def userButtonPressed(which_pin):
    '''
    @brief This function runs when the button is pressed
    @details This is the external interrupt function that runs when the
    user button is pressed on the Nucleo. In our case during
    this lab, the button initiates recording temperature from its i2c device!
    The data will be overwritten if pressed after an initial recording!
    '''
    global recording #pull in the global boolean
    if recording == False: #make sure the global recording variable is false 
                           #and that the Nucleo isnt recording!
        global init_time #introduce global float to userButtonPressed()
        init_time = pyb.millis()
        print('Okay! We will begin recording your temperature for 8 hours!')
        recording = True
        
    elif recording == True: #if already recording
        print('Hey! You already are recording data!')


#Initialize the user button!
myButton = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, userButtonPressed)
    #The blue user button is bound to pin C13 (pyb.Pin.cpu.C13)
    #The button has a falling edge when triggered (pyb.ExtInt.IRQ_FALLING)
    #Enable the pull up resistor (pyb.Pin.PULL_UP)
    #Declare the timeChecker function to run on the interrupt!
    
##----------------------------Define i2c object-----------------------------##

mySensor = MCP9808(24) #address, MCP9808 class, initialize it!
#This sensor has built in functions in the MCP9808.py file

##-------------------------------Run Tasks----------------------------------##

print('Welcome, my friend! \nLets see if its hot or not! \nPress the blue button on the Nucleo to record temperature!')

with open(filename, 'w') as csv:
    header = 'Time (s), Core Temperature (*C), Ambient Temperature (*C)\n'
    csv.write(header)

    while True:
        #Note the current time
        curr_time = pyb.millis() # [seconds]
        
        #check recording status
        try:
            if recording == True and curr_time-last_time >= rec_resolution:     
    
                #gather the data
                tempAmbi = round(mySensor.celsius(),2)
                tempCore = round(adc.read_core_temp(),2)
                time = int((curr_time - init_time)/1000) ## [s], relative recording time
                
                #Add the data to the csv file on the Nucleo
                print(str(time) + ' [s], ' + str(tempCore) + ' [C], ' + str(tempAmbi) + ' [C]')
            
                # Data string
                data = str(time) + ', ' + str(tempCore) + ', ' + str(tempAmbi) + '\n'
                
                #Write data to csv file
                csv.write(data)
            
                #reset the last time to curr time
                last_time = curr_time
                
                pyb.delay(1000)
                
                if time > 3600*8:
                    KeyboardInterrupt
                    
            if recording == False:
                pass
            
        except KeyboardInterrupt:
            hrs = round(time/3.6E6,2)
            recording = False
            print('Data Collection continued for ' + str(time) + ' seconds, or ' + str(hrs) + ' hours. \n\n' )
            break
        
        
