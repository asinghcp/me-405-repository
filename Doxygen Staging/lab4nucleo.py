'''
@file lab4nucleo.py
@brief This file calls the driver for the temperature sensor and executes data collection
@details ...
@author Anil Singh & Benjamin Presley
@date February 11, 2021
'''
from pyb import I2C
import utime
import pyb
from MCP9808 import MCP9808

## Initialize ADCAll Object
adc = pyb.ADCAll(12, 0x70000)

#Run a vref check to fix strange error with temp read
adc.read_vref()

## Initialize Data FileName
filename = 'Nucleo_Temperatures.csv'

## Initialize Start Time to count total # of milliseconds since beginning of data collection
ticks_0 = utime.ticks_ms()

## Iterating ticks value (should start identical to ticks_0)
t_iter = utime.ticks_ms()

## Initialize MCP9808 Driver
MCP9808 = MCP9808(24)

## Initial Time in integer for for algebra calcs
t0 = 0

print('Recording Temperature Data from MCP9808')
      
with open(filename, 'w') as csv:
    header = 'Time (s), Core Temperature (*C), Ambient Temperature (*C)\n'
    csv.write(header)
    while True:
        try:
            #Read Core Temp
            temp= adc.read_core_temp()
            #Read ambient temp off MCP9808
            temp_A = MCP9808.celsius()
            #Get Current Ticks
            t_current = utime.ticks_ms()
            #Calculate time from start
            time_ms_run = utime.ticks_diff(t_current, t_iter)
            #Update Cycle Time
            t_iter = t_current
            #Update Net time
            t0 += time_ms_run
            ## Iteration time in seconds
            time_s = t0/1000
            ## Data string (changes every iteration)
            data = str(time_s) + ', ' + str(temp) + ', ' + str(temp_A) + '\n'
            #Write data to csv file
            csv.write(data)
            #sleep until next second
            utime.sleep(60)
        except KeyboardInterrupt:
            total_time = utime.ticks_diff(utime.ticks_ms(), ticks_0)
            hrs = total_time/3.6E6
            print('Data Collection continued for ' + str(total_time/1000) + ' seconds, or ' + str(hrs) + ' hours.' )
            break
        
print('The .csv file has been closed and is stored on the Nucleo as ' + filename + '.')