'''
@file mainpage.py

@mainpage

@image html nucleo.png width=600in

@section sec_intro Introduction
This website hosts documentation related to Anil Singh's ME305 repository located 
at:
    https://bitbucket.org/asinghcp/me-405-repository/src/master/  

@section sec_references References
The documentation and code displayed at this site was developed in conference
with the following engineers:\n
    --Kai Quizon, EIT (https://kquizon.bitbucket.io)\n
    --Jacob Winkler (https://jcwinkle.bitbucket.io)\n
    --Rick Hall (https://rhall09.bitbucket.io/)\n
    \n
The included code and documentation would be impossible without the help and
guidance of:\n
    --Charlie Refvem, Lecturer, Cal Poly SLO\n
    --Dr. Eric Espinoza-Wade, Professor, Cal Poly SLO\n
    --Dr. John Ridgely, Professor, Cal Poly SLO\n
    
@section sec_directory Directory
1. \ref page_vendotron
2. \ref page_reaction_time
3. \ref page_ADC
4. \ref page_temp
5. \ref page_Feeling_Tipsy
6. \ref page_simulation_or_reality
7. \ref page_Feeling_Touchy
8. \ref page_termI
9. \ref page_TPH
10. \ref page_TPS
11. \ref page_TPR

@page page_vendotron Vendotron (Lab 1)

@image html vendgif.gif width=600in

@section page_vendotron_desc Description
The code on this page documents a program housed within Spyder to run a
virtual vending machine called vendotron. vendotron is able to accept currency
represented by keyboard presses between 0 and 7, representing values ranging
from pennies to twenties, respectively. The machine will compute the current
balance, handle transactions for four drink items, and will dispense the
item as well as the change. At any point the transaction can be aborted and
the current balance will be ejected.  

@section page_vendotron_sys State Transition Diagram

@image html lab1diagram.png

@section page_vendotron_src Source Code Access
Source Code related to Lab 1 is located at: https://bitbucket.org/asinghcp/me-405-repository/src/master/Lab%201/

@section page_vendotron_doc Documentation
Documentation related to Lab 1 is located at \ref lab1_vendotron.py


@page page_reaction_time Reaction Timer (Lab 2)

@section page_reaction_time_desc Description
The code on this page documents a program housed on the nucleo to test a
user's reaction time. The user is able to respond to an LED lighting up by
using the push button. The program will record the reaction time taken 
(in milliseconds) for the user to push the button and display the average
reaction time.  

@section page_reaction_time_src Source Code Access
Source Code related to Lab 2 is located at: https://bitbucket.org/asinghcp/me-405-repository/src/master/Lab%202/

@section page_reaction_time_doc Documentation
Documentation related to Lab 2 is located at \ref lab2_reactiontime.py

@page page_ADC ADC Voltage Response to User Button (Lab 3)

@section page_ADC_desc Description
The code on this page documents a program housed on the nucleo and within 
Spyder that tests the ADC voltage response of the user push button on the 
nucleo. The program will prompt the user to enter a start command "g", after 
which they will be prompted to push the user button on the nucleo. There are 
2 main files to run this program. The first is the file housed on the nucleo, 
which serves to compile voltage data from the user push button and comminicate 
that data to the UI. The second is the UI housed within Spyder, which is 
designed to communicate with a the \ref main.py file on the nucleo to compile 
the voltage response, plot the response against time, and export the response 
to a .csv file for further analysis.

@image html lab3plot.png "Sample ADC Voltage Response Generated by Program" width=600in

@section page_ADC_src Source Code Access
Source Code related to Lab 3 is located at: https://bitbucket.org/asinghcp/me-405-repository/src/master/Lab%203/

@section page_ADC_doc Documentation
Documentation related to the UI for Lab 3 is located at \ref lab3UI.py \n
Documentation related to the nucleo housed code for Lab 3 is located at \ref main.py

@page page_temp MCP9808 Temperature Measurement (Lab 4)

@section page_temp_desc Description
The code on this page documents a program housed on the nucleo. The program is
designed to utilize the MCP9808 sensor to accurately measure temperature over
time. The sensor is run by the nucleo and measures both ambient and core 
temperature. The program then compiles the temperature data as a .csv file 
and saves it to the nucleo for export and data analysis. Below is a sample 
output of the system over a displayed time interval. This code was generated
in collaboration with Ben Presley, in addition to the usual collaborators
listed on the refrences section of this page. However, data was collected and
showcased individually. Eacvh of us used seprate main files, but achieved 
the same results with identical drivers. 

@image html lab4plot.png "Ambient and Core Reading Temperatures vs Time" width=600in

@section page_temp_src Source Code Access
Source Code related to Lab 4 is located at: https://bitbucket.org/asinghcp/lab-4-shared/src/master/

@section page_temp_doc Documentation
Documentation related to the Driver for lab 4 is located at \ref MCP9808.py \n
Documentation related to the nucleo housed code for Lab 3 is located at \ref lab4nucleo.py \n

@page page_Feeling_Tipsy Feeling Tipsy? (Lab 5)

@section page_Feeling_Tipsy_desc Description
This lab details the derivation of a two dimensional analytical model of the
ball balancing platform. Part one shows a schematic of the 4 bar linkage shown 
in Figure 4 of the lab manual. Part two illustrates the kinematic relationship 
between the motion of the platform and the motion of the ball. \n
\n
These figures were developed in conference with Kai Quizon. A link to his page
is listed here: https://kquizon.bitbucket.io/page_Lab5.html 

@image html lab5part1.png "Lab 5 Part 1 - Drawn by Kai Q." width=600in \n
\n
@image html lab5part1a.png "Lab 5 Part 2 - Drawn by Kai Q." width=600in \n
\n
@image html lab5part2.png "Lab 5 Part 3 - Drawn by Kai Q." width=600in \n
\n
@image html lab5part3.png "Lab 5 Part 4" width=600in \n
\n
@image html lab5part4.png "Lab 5 Part 5" width=600in \n
\n
@image html lab5part5.png "Lab 5 Part 6" width=600in

@page page_simulation_or_reality Simulation or Reality? (Lab 6)

@section page_simulation_or_reality_desc Description
In this lab, the state space models derived in lab 5 were simulated in MATLAB
to produce a simulated system response to varying cases of initial conditions 
and inputs. A feedback control loop was implemented to obtain the system
response to a controls system.\n
These figures were developed in conference with Kai Quizon, Rick Hall, and
Jacob Winkler.

@section page_simulation_or_reality_hc Hand Calculations: Linearization
The model was linearized for ease of simulation in MATLAB. This was 
accomplished using the Jacobian Linearization method. \n

@image html lab6pt1.png width=600in \n
\n
@image html lab6pt2.png width=400in \n
\n
@image html lab6pt3.png width=300in \n
\n
@image html lab6pt4.png width=400in \n
\n
@image html lab6pt5.png width=600in \n
\n

@section page_simulation_or_reality_MATLAB MATLAB Outputs

@image html abmat.png Simulated A and B Matrices width=600in
@image html statespace.png State Space Variable width=600in
@image html bd.png Closed Loop Block Diagram width=600in
\n
@image html lab6pt6.png Open Loop Case 1 Response width=600in \n
Case 1 places the ball in the equilibrium position with no initial conditions.
The model behaves as expected, remaining undisturbed. \n
@image html lab6pt7.png Open Loop Case 2 Response width=600in \n
\n
Case 2 offsets the equilibrium point by 5cm with no initial platform velocity,
tilt or motor torque. Past 0.2s, the ball's weight results in the platform 
tipping and the ball rolling off. \n
@image html lab6pt8.png Open Loop Case 3 Response width=600in \n
\n
Case 3 places the ball at the equilibrium point with no initial velocity. The 
platform is tilted by an initial five degrees, but has no angular velocity. 
The motor provides no torque. The response shows the anticipated result.
The platform acts as a ramp, with the ball accelerating down an incline. As
the ball's weight causes the platform to tip further, the acceleration becomes
nonlinear. \n
@image html lab6pt9.png Open Loop Case 4 Response width=600in \n
\n
Case 4 places the ball at the equilibrium point with no intial velocity. The 
platform has no initial tilt or angular velocity. The motor provides an 
impulse torque of 1 mN*mm at time 0. There is a clear linear portion in the 
angular velocity response that relates to the applied torque of the motor. 
The remaining results are similar to case 2. \n
@image html lab6pt10.png Closed Loop Response width=600in \n
\n
The closed loop case emulates the initial conditions of Open Loop Case 2. 
However, the closed loop uses a precalculated feedback loop with a feedback 
gain as follows: K = [-0.05 -0.02 -0.3 -0.2]. The controller behaves as a 
second order system, showing ocillitory behavior until equilibrium is reached. 
This indicates an unoptimized, but marginally stable system. \n

@section page_simulation_or_reality_src Source Code Access
Source Code related to Lab 6 is located at: https://bitbucket.org/asinghcp/me-405-repository/src/master/Lab%206/

@page page_Feeling_Touchy Feeling Touchy? (Lab 7)

@section page_Feeling_Touchy_desc Description
In this lab, a driver to operate the ER-TFT080-1 Resistive Touch Panel used in
the term project. The driver includes several methods, including the ability
to read each cartesian component of feedback from the panel, as well as a
method to return the combined cartesian response as a tuple. 

@image html touchpad_over.jpg Overhead View of ER-TFT080-1 Resistive Touch Panel width=600in

@section page_Feeling_Touchy_src Source Code Access
Source Code related to Lab 7 is located at: https://bitbucket.org/asinghcp/me-405-repository/src/master/Lab%207/

@section page_Feeling_Touchy_doc Documentation
Documentation related to the Driver for lab 7 is located at \ref touchdriver.py \n

@page page_termI Term Project Part I (Lab 8 & Term Project)

@section page_termI_desc Description
The code on this page documents the motor and encoder drivers used in the 
platform balancing system for the Term Project. The Encoder driver is
responsible for measuring the encoder angle (and subsequently the motor angle)
and returning the angular position. The driver is contained within a Python
class. The Motor Driver is responsible for setting up the neccesary pins and
controlling the power to drive each motor in the system. This driver is also
contained in a Python class.\n
\n
These drivers were developed in conference with Kai Quizon. Links to his 
repository are provided below.

@section page_termI_link Detailed Documentation
Detailed documentation can be found on Kai Quizon's portfolio page located here:
https://kquizon.bitbucket.io/page_Lab8.html
@section page_termI_src Source Code Access
Source Code related to Lab 8 is located at: https://bitbucket.org/kquizon/me-405/src/master/Lab8//

@section page_termI_doc Documentation
Documentation related to the Encoder Driver is located at \ref Encoder.py \n
Documentation related to the Motor Driver is located at \ref MotorDriver.py

@page page_TPH Term Project Hardware (Term Project)

@section page_TPH_Desc Description
This page outlines the hardware setup used in the term project. We will trace the command line
and sensor feedback of the entire system and establish the basic functionality of each component,
referencing it's corresponding driver.

@section page_TPH_Controller Controller Mounts
The system is controlled by a Nucleo L476RG Microcontroller. The Nucleo stores
the control files and hosts teh output csv file. The Nucleo is mounted on a 
custom PCB created by Charlie Refvem (CC BY-NC-SA 4.0). This PCB contains solder
points for all the GPIO pins of the nucleo, as well as screw terminals for the 
motor inputs and ports for the two encoders. The PCB is also capable of interfacing
with the DSD Tech HM19 Bluetooth chip, but this port is not utilized for this project.\n
\n
The board also has a USB Type B port to communicate with the Nucleo. This port hosts
a virtual flash drive that allows files to be "dragged and dropped" instead of imported
using the ampy module. Finally, the board features a port for connecting to a 12VDC power
source. This external power source allows the entire system to be run independently from
a computer, assuming that the file desired to be run is correctly installed on the Nucleo.\n
\n
The complete system with all applicable connections established is shown below.

@image html TPMCU.jpg Microcontroller Mount and PCB width=600in

@section page_TPH_Motor Actuators

@image html TPMotor.jpg DCX22S 12VDC Step Motor in Term Project Hardware Mount width=600in

The actuators in this system are two DCX22s 12VDC Stepper Motors (shown above,controlled by \ref MotorDriver.py). These stepper motors are
connected via a square toothed belt to a larger gear attached to an output shaft. The gear ratio
between the motor and the output shaft is measured to be approximately 4.3 (NOTE: The encoder is
attached to the output shaft.) The output shaft is then connected to a lever arm.
This lever is mounted via a plastic/metal ball joint to a push rod. 
This push rod has a second ball joint at its mounting location to the top plate. 
This is a rigid connection, the screw is not allowed to translate in the slot attached
to the top plate. This allows the motion of the motor to be translated into 
corresponding motion of the top plate. The motor lever has a length of 110mm
while the push rod is 60mm from joint to joint. This gives the conversion
from output shaft angle to top plate angle (60/110). 

@section page_TPH_Encoder Sensor 1: Output Shaft Encoder

@image html TPEncoder.jpg Encoder Attached to Output Shaft in Term Project Hardware width=600in

Attached to the output shaft is a E4T-1000-236-S-D-M-B encoder, shown above
in its mounted position. This is the closer sensor to the actuators and can be 
used to determine the angle of tilt of the platform. The encoders are driven by
the encoder driver class (\ref Encoder.py). This encoder is attached to the PCB
and provides real time readings to the Nucleo, as called by the software. The
output of the encoder is multiplied by the conversion factor determined by the geometry
of the system (60/110,described above) to determine the tilt angle of the top plate
in a specific dimension. The x encoder is related to the tilt about the y axis, and
the y encoder is related to the tilt about the x axis.
\n
These encoders may be used in liu of the BNO055 IMU if desired, but have several drawbacks.
The largest drawback comes with initialization. Upon startup, the encoders return a 
zero, regardless of their true position. This requires that the system be initialized
in the balanced position, otherwise there will exist a steady state error related
to the intiial tilt of the platform. Further, because the encoders are attached
to the output shaft, any slippage of the belts results in unreliable, meaningless
encoder readings. Generally, this shows the superiority of the IMU for collecting
top plate movement and angles.

@section page_TPH_IMU Sensor 2: BNO055 IMU

@image html TPIMU.jpg The BNO055 IMU Mounted to the Top Plate width=600in

The BNO055 IMU (shown above) has a sensor suite capable of measuring position in quaternions,
heading, gyroscopic rotation, linear velocity and acceleration, and the gravity vector. 
Using the gravity vector,the heading, and gyroscoping rotation, the angle of tilt
of the top plate and the angular velocity of the top plate may be determined.
Because these state variables are measured directly by the IMU, it is much more
reliable than the Encoder method, as described above. The BNO055 communicates
with the Nucleo via I2C protocols.

@section page_TPH_touch Sensor 3: Touch Panel

@image html TPOverhead.jpg Overhead Shot of Touch Panel Mounted to Top Plate width=600in

The third sensor utilized to record the position of the ball along the top plate is
an ER-TFT080-1 Resistive Touch Panel (shown above). The resistive touch panel
functions as a voltage divider with varying voltages depending on the location of the
ball in the x and y planes. This sensor is the slowest of those used in this project,
taking approximatley 1500 us to read all three dimensions as pins must be reassigned.
The RTP communicates with the Nucleo via four GPIO pins. Because the RTP's output
is a printed flexible circuitry strip, a converter chip is required between the
RTP and the Nucleo pins (shown below).

@section page_TPH_function Hardware Functionality
@image html TPISO.jpg Isometric View of Full System width=600in
From a top level perspective, the Nucleo takes reading from two sensors and commands
a duty cycle to the motors, resulting in a change in the angular position of the
top plate and linear position and velocity of the ball. The system functions using
EITHER the Encoders or the IMU, not both. 

@page page_TPS Term Project Software (Term Project)

@section page_TPS_desc Description
This page discusses the general layout and functionality of the software used
to drive the Term Project Hardware, who's main script is contained in \ref projectFile.py

@section page_TPS_src Source Code Access
The source code for the term project may be accessed here: https://bitbucket.org/asinghcp/me-405-repository/src/master/Term%20Project/

@section page_TPS_drivers Drivers
During operation, the term project code calls three drivers. However, because
the system may be run with either the encoders or the IMU, four drivers are described
below.\n
\n
\ref MotorDriver.py controls the motor action of the system. The driver allows
for the enabling and disabling of motors, as well as the feeding of a duty cycle
of PWM to drive the motors at a particular velocity in a particular direction.
More detailed information on the \ref MotorDriver.py may be viewed in \ref page_Lab8.\n
\n
\ref touchdriver.py interacts with the Resistive Touch Panel. It handles the creation
of and reassignment of pins in order to efficiently read the three dimensions of
the touch panel. This driver directly provides two of the state variables of the
system, x and y. When called with the timing of the task, the velocity of the ball
(xdot and ydot) may also be derived. All of these readings are in relation to the center of the touch panel
as calibrated during \ref page_Lab7 \n
\n
\ref Encoder.py handles the outputs of the Encoders attached to the output shaft.
The driver is capable of returning both the position of the encoder and the delta,
which when called with the timing of the task, may be used to find two more state variables:
the angle of tilt of the top plate and the angular velocity of the top plate. More
information on the encoder driver may be read in \ref page_Lab8 \n
\n
\ref bno055.py handles the outputs and calibration of the BNO055 IMU. This driver
was written by Radomir Dopieralski for Adafruit industry and is used under the
MIT License (MIT), found via the open source platform github. The document is unmodified, 
with the exception of the addition of doxystring comments for clarity. The driver
allows for the return of the heading and gyroscopic motion of the top plate,
directly providing four state variables: thetax, thetay, thetax_dot, and thetay_dot.

@section page_TPS_fileshare Task Queueing and Priority Organization
The main script for controlling the hardware uses a priority based cooperative
task execution scheme developed by Prof. John Ridgely and used under the GNU Copyright V3.0. This cooperative multitasking
is executed through two main files: \ref cotask.py and \ref task_share.py.\n
\n
\ref cotask.py allows for the creation of task objects with a user assigned priority and period. Then,
a priority scheduler is called on the list of task objects. The priority scheduler checks
which tasks are ready to be executed based on their period and then executes the ready
tasks in order of priority. In this lab, the necessary actions were divided into 6 tasks:\n
\n
1. X Axis Controller\n
2. Y Axis Controller\n
3. Touch Panel Data Collection\n
4. IMU Data Collection\n
5. Encoder Data Collection\n 
6. Write Data to CSV\n
\n

While there are six possible tasks, only five tasks are ever executed as the system
is run either on the IMU or the encoders. The tasks above are listed in the order
of assigned priority, with the controllers having the highest priority and write data
the lowest. A description of each task is below.\n
\n
X Axis Controller: The X Axis Controller takes data from each of the queues,
performs mathematic operations to convert units (degrees to rads) and then uses
the state space feedback model (T= K*X, where X is the state space matrix) to 
determine the necessary torque for the system correction. The necessary torque
is converted to a duty cycle using the motor characteristics. The duty cycle is
then fed to the motor. The X axis controller has the highest priority and is
executed with a period of 2.5ms.\n
\n
Y Axis Controller: The Y Axis Controller takes data from each of the queues,
performs mathematic operations to convert units (degrees to rads) and then uses
the state space feedback model (T= K*Y, where Y is the state space matrix) to 
determine the necessary torque for the system correction. The necessary torque
is converted to a duty cycle using the motor characteristics. The duty cycle is
then fed to the motor. The Y axis controller has the second highest priority and is
executed with a period of 2.5ms.\n
\n
Touch Panel Collection: Touch Panel collection calls the \ref touchdriver.py to 
collect data from the touch panel. It then process that data using time stamps
to produce linear velocity data for the ball. It then stores the position and velocity
of the ball in the appropriate queues. The Touch Panel Collection task has the highest
priority of the sensor tasks and is executed with a period of 2.5 ms.\n
\n
IMU Data Collection: IMU Data collection calls the \ref bno055.py driver and collects
heading and gyroscopic data from the IMU. The data needs to postprocessing and is
immediately stored into the appropriate queues for the angular position and velocity
of the top platform.The IMU Data Collection task is equal priority to the Encoder task
and the lowest of the sensor tasks. It is run with a period of 5ms. \n
\n
Encoder Data Collection: Encoder Data Collection calls the \ref Encoder.py driver
and collects the angular position of the output shaft from the encoder. This data
is processed to the angular position and acceleration of the platform using the geometry
of the system and a time stamp in the Encoder Data Collection task. The processed
data is then stored in the appropriate queue. The Encoder Data Collection task is 
of equal priority to the IMU Collection task and the lowest of the sensor tasks.
It is run with a period of 5ms. \n
\n
Write Data to CSV: If enabled, the Write Data to CSV task opens a csv file and 
writes the specified data (four of the eight state variables) to the csv file.
It is the lowest priority task called at the highest interval of 50ms.\n
\n
\ref task_share.py allows for the creation of Queue and Share objects such that data and booleans 
may be shared between various tasks in the task list. Queues function as arrays with
a moving pointer that keeps track of data present and what may be accessed. Shares
are single value variables that may be accessed by any task. The core functionality
of the \ref task_share.py is to handle the disabling and enabling of interrupts and other
functionality that may corrupt data if called during data accessing. For this project,
each state variable (as seen in \ref page_Lab5) is assigned a queue. There is only one
share variable (fault) which describes the state of the motor. If fault is 1, then
a fault has been detected and no controllers will be run. A task diagram of how
data is shared between the tasks called is shown below. Note that the encoder
and IMU tasks create the same data and as such only one is called when the code
is executed.

@image html TPTaskShare.png Variables Shared Between Tasks width=600in

@section page_TPS_Fault Fault Handling
To prevent damage to hardware or potential injury, the code is built with fault
detection and handling. Using the DCX22's built in nFAULT pin (which falls low if
an overcurrent condition is detected), an external interrupt was built. The external
interrupt stores data into a Shares variable and disables the motors. Processing power
is saved by disabling the controllers while the fault is live. The fault may be cleared
by the user in the REPL terminal when conditions are again clear.

@section page_TPS_Functionality General Functionality
A top level via of the code may descirbed by the following steps:\n
1. Initialize hardware, queues, share variables, and task objects\n
2. Initialize the priority scheduler and interrupt for faults\n
3. The priority scheduler runs each of the five tasks in order of priority and period:\n
4. Sensors collect data and store it in the appropriate queues\n
5. The controller runs with the updated data from the queues\n
6. Steps 4 and 5 are repeated ad infinity until user input or a fault is detected.

@section page_TPS_User User Manual
To successfully operate the tilt table using the software described above, a 
user must make four major decisions. These booleans, contained under the if name = main
portion of \ref projectFile.py control which aspects of the system are run. The first
two booleans enable or disable individual axis controllers. The third boolean allows
the user to choose between measuring table angle from the encoders or the BNO055 IMU.
The final boolean toggles data collection. After choosing which tasks to run,
the user simply loads and executes the file. No input is needed to start control.
It is recommend that the user wait for control to be initiated before placing the ball.
The user may then place the ball atop the platform and observe the system response.
If a motor overcurrent fault is detected, the user may restart the system when the
hardware is clear by typing "Fault Cleared" into the REPL command line.

@page page_TPR Term Project Results (Term Project)

@section page_TPR_desc Description
This page discusses the results of the completed code, including tuning and
final ball balancing.

@section page_TPR_tuning Tuning the Table
First, the table was tuned to balance itself without any input to the touch
panel. Initially, user inputs were used to disable one axis allowing the tuning
of a single axis at a time. This had advantages in simplicity, but disadvantages
as the assumption that the axes are uncoupled is not entirely true. Below, one
may view tuned results of disturbance tests to the x and y axes.

@image html XAxisDisturbanceTest.png width=600in
@image html YAxisDisturbanceTest.png width=600in

/n
We see from both of these tests that for the gains and nonlinear parameters selected,
the top plate exhibits a "dead zone" where it is not perfectly zero, but unable
to correct itself due to the dead zone in the motors. This had to be introduced 
as removing a dead zone prevented stability from being achieved, leading to jittery
behavior that disrupted the ball and lead to instability. As such, the above
tests were deemed acceptable./n
/n
A video of these disturbance tests with both axes enabled is at the link shown below. We see
that the table responds well without input from the touch panel and is very stable
for small angles, but does begin to oscillate slightly with larger angles (as expected
since our model utilized the small angle approximation).\n
\n
https://www.youtube.com/watch?v=0C4BL-Nb0OA
\n

@section page_TPR_balancing Balancing the Ball
Ultimately, due to both the variations in hardware, sensitivity to gain values,
and general unpredictability of the rolling ball, balancing the ball was only
achieved sporadically. However, some of these sporadic successes were captured
in various means. Below, the x and y response of a short, but successful test
are shown. The data markers represent the collection interval (approximately 50 ms),
while the interpolated line shows the rough movement of the ball. We see the expected
behavior in x, and some coupling in y. The x behavior is nearly a textbook second order
response. The system overshoots, then we see the ball oscillate slightly and settle
in the "dead zone" (described above). The y response is slightly different. Because
the two axes are coupled, we see the ball remains in the positive y for the entire
test, but still settles in the dead zone when the platform balances again.

@image html XResponseGraph.png width=600in
@image html YResponseGraph.png width=600in

The video in the link below shows the primary behavior of the fully functional system. The
system is largely stable for small angles of the platform and small velocities of
the ball. However, once the ball begins to move too quickly or the platform tilts
too far, the system is unable to respond, the ball is dropped. Once the ball drops
off the platform, the platform returns successfully to zero. All of this behavior
shows that the system is performing correctly, we even see the ball tend towards
zero in all instances. Repeatedly balancing the ball in various circumstances
simply requires further tuning the gains and nonlinear parameters of the system.
We call this MISSION SUCCESS!\n
\n
https://www.youtube.com/watch?v=7MbAT1GizgQ
\n
@section page_TPR_partner Partnership
This term project was completed in partnership with Kai Quizon, who's documentation may be viewed here:
https://kquizon.bitbucket.io/index.html \n
Considerable work was also done on the term project in conference with Rick Hall
and Jacob Winkler. The results displayed above are a joint effort between these four
engineers.
'''