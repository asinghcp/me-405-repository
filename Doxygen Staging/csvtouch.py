"""
@file csvtouch.py

@brief Simple script to ...
@detail This script ...

@author Anil Singh
@author Kai Quizon
"""

import pyb
import utime
from touchdriver import touchdriver
import array as ar

tch = touchdriver('PA0','PA6','PA1','PA7')

tBuf = ar.array('h',[])

with open ("csvtouch.csv", "w") as csvFile:
    csvFile.write('Time [s], Xpos [bits], Ypos [bits], Zpos[bits]\n')
    while True:
        try:
            input('Press Enter to Start Data Collection \n')
            while True:
                t0 = utime.ticks_us()
                tch.read()
                t1 = utime.ticks_us()
                tdif = utime.ticks_diff(t1,t0)
    
                line = str(tdif) + ', ' + str(tch.pos[0]) + ', ' + str(tch.pos[1]) + ', ' + str(tch.pos[2]) + '\n'
                csvFile.write(line)
            
        except KeyboardInterrupt:
            print('\n'*50 + 'Test Finished\n\n')
            break 