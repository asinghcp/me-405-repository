'''
@file mainpage.py

@mainpage

@section sec_intro Introduction
Welcome! This website hosts documentation related to the repository located 
at:
    https://bitbucket.org/kquizon/me305_labs/src/master/

@section sec_directory Directory
Homework (Lecture) Assignments
1. \ref page_Homework1

Laboratory Assignments
1. \ref page_Lab1
2. \ref page_Lab2
3. \ref page_Lab3
4. \ref page_Lab4
5. \ref page_Lab5
6. \ref page_Lab6
7. \ref page_Lab7
8. \ref page_Lab8
9. \ref page_TPH
10. \ref page_TPS
11. \ref page_TPR

@section sec_references References
The documentation and code displayed at this site was developed in conference
with the following engineers:\n 
    --Anil Singh (https://asinghcp.bitbucket.io)\n 
    --Jacob Winkler (https://jcwinkle.bitbucket.io)\n
    --Rick Hall (https://rhall09.bitbucket.io)\n
\n
The included code and documentation would be impossible without the help and 
guidance of:\n 
    --Charlie Revfam, Lecturer, Cal Poly SLO\n 
    --Dr. Eric Espinoza-Wade, Professor, Cal Poly SLO\n
    --Dr. John Ridgely, Professor, Cal Poly SLO


@page page_Homework1 Homework 1: Calculating Change

@section page_Homework1_Description Description
In this assignment, it was desired to calculate the minimum piece (coinage and bill) method
for distributing change when provide with a payment and price of item. The python code
does not require any hardware and may be executed directly in the shell as a function.

@section page_Homework1_src Source Code Access
You can find the Source Code for the Change Exercise here: https://bitbucket.org/kquizon/me-405/src/master/Homework/


@page page_Lab1 Lab 1: VendoTron
@image html Lab1Ref.png "Created By Charlie Revfem" width=600in

@section page_Lab1_desc Description
In this lab, the code governing the operations of Vendotron (shown above) is written to \ref Lab1.py.
The state machine utilizes one prebuilt "get change" function (\ref HW1.py) and the keyboard module.
The code is built as a finite state machine governed by the following state diagram:
    
@image html Lab1SD.png width=600in

@section page_Lab1_src Source Code Access
You can find the Source Code for the Change Exercise here: https://bitbucket.org/kquizon/me-405/src/master/Lab1/

@page page_Lab2 Lab 2: Test Your Reflexes

@section page_Lab2_desc Description
In this lab, it was desired to set up a basic reaction test using onboard components
of the Nucleo L476 Dev Board. At a random interval between 2 and 3 seconds, the 
onboard LED would light up and a timer would start. The timer stops when the 
participant presses the onboard user interaction button. These reaction times are
stored for any number of desired tests and the average reaction time is presented
when the user ends the test by pressing ctrl-c. The entire program was built
as a single execution file using an interrupt in \ref Lab2.py.

@section page_Lab2_src Source Code Access
You can find the Source Code for the Change Exercise here: https://bitbucket.org/kquizon/me-405/src/master/Lab2/

@page page_Lab3 Lab 3: ADC Voltage Conversion and Serial Communication

@section page_Lab3_desc Description
In this lab, interrupt code generated in \ref page_Lab2 was expanded upon to practice
serial communication, ADC data collection, and plotting. A command was sent from the PCUI
interface (hosted in Spyder as \ref Lab3PCUI.py). When the Nucleo (running \ref Lab3Nuke.py) received this command,
it allowed for the collection of ADC data off of pin A5, which was wired directly to
the user pushbutton pin (pin PC13). The data was prescreened to account for the difference
in timing between the code and the electric response of the system. The screened data
was then sent back to the \ref Lab3PCUI.py where it was plotted utilizing the pyplot module.

@section page_Lab3_src Source Code Access
You can find the Source Code for the Change Exercise here: https://bitbucket.org/kquizon/me-405/src/master/Lab3/

@section page_Lab3_results Results
The final results of this lab experience were a graph of ADC voltage response vs time
and the corresponding data saved in a .csv file. The voltage graph may be seen below.
Beneath the voltage graph is a link to the corresponding .csv file.
@image html ADCVoltageOutputGraphFinal.png width=600in
https://bitbucket.org/kquizon/me-405/src/master/Lab3/ADC%20Voltage%20Output20210203-222315.csv

@page page_Lab4 Lab 4: Hot or Not?
@image html MCP9808.jpg Copyright: Adafruit Industries width=600in

@section page_Lab4_desc Description
In this lab, it was desired to compare the temperature read off of the Nucleo
L476's internal MCU temperature sensor with the data read off an external sensor,
in this case Adafruit Industry's MCP9808 as shown above. A driver for the MCP9808
was written (\ref MCP9808.py) to communicate with the MCP9808 via the I2C communication scheme. This driver was called by a script (\ref Lab4Nuke.py) that sampled 
the ambient temperature as measured by teh MCP9808 and the core temperature (from
an ADCAll object). These temperatures were saved in a .csv file locally on the Nucleo.
The file was then retrieved after the script was run for 9 hours using ampy. The data
was then plotted in Spyder using the \ref Lab4Plot.py script.

@section page_Lab4_src Source Code Access
You can find the Source Code for the Change Exercise here: https://bitbucket.org/kquizon/me-405/src/master/Lab4/

@section page_Lab4_collab Collaboration
This Lab was completed in collaboration with partner Logan Garby. The above
repository was shared between myself and Mr. Garby. It is important to note
that our data was collected individually in separate environments.

@section page_Lab4_results Results
@image html TempGraph.png width=600in
The above data was collected in an internal, temperature controlled room located
near a running computer. The house's central heating was turned on shortly after
startup and ran for a majority of the data collection (as can be see by the uptick
in data at the beginning of collection).

@page page_Lab5 Lab 5: State Space Model of Tilt Table System
@image html TiltTable.png Tilt Table Simplified model, Copyright: Charlie Revfem width=600in

@section page_Lab5_Desc Description
In this lab, it was sought to derive the governing equations of the system shown
above. These equations are presented in a series of hand calculations shown below
and ultimately placed in state space form for future simulation. Note that the small
angle approximation is used liberally in this derivation. Therefore large angles of
tilt of the platform will result in dynamics not predicted by the governing equations below

@section page_Lab5_Partner Partnership
I partnered with the wonderful Anil Singh to complete this derivation.

@section page_Lab5_HC Hand Calculations
First, we derived kinematic relationships between the tilt table and the motor arm:
@image html LeverArm_Kinematics.jpg width=600in

Furthering the Kinematics analysis, we derived a relationship between the tilt of the
table and the motion of the ball on the table:
@image html Ball_Kinematics.jpg width=600in

Having solved the kinematic relationships, we continued on with Kinetic analysis
of the ball and platform relationship:
@image html Ball_Kinetics.jpg width=600in

Moving "down" the system we investigated the kinetics of the platform, replacing
the lever arm assembly with unknown forces at point Q:
@image html Platform_Kinetics.png Written By: Anil Singh width=600in

Finally, we solved the relatively simple kinetics of the motor lever arm.
It is important to note that a major simplifying assumption is made here: the "push arm"
is modeled as a two force member as any force in the horizontal direction will be absorbed
by bearings.
@image html LeverArm_Kinetics.png width=600in

We were then able to eliminate the unknown forces at Q by setting the last two steps
equal to Q and substituting out. From there, we simplified down to the final state space
model:
@image html AccelSolves.png Written By: Anil Singh width=600in
@image html State_Final.png Written By: Anil Singh width=600in

@page page_Lab6 Lab 6: Simulation or Reality?
@image html TiltTable.png Tilt Table Simplified model, Copyright: Charlie Revfem width=600in

@section page_Lab6_Desc Description
In this lab, the state space equations produced in Lab 5 were simulated using
MATLAB to produce the dynamic response of the system to various initial conditions
and inputs. Finally, a primitive control loop was tested to view the basic response
of a controlled system. The system is once again shown above.

@section page_Lab6_src Source Code Access
You can access the MATLAB and Simulink files used here: https://bitbucket.org/kquizon/me-405/src/master/Lab6/

@section page_Lab6_Linear Linearization
Before importing the state space model to the MATLAB workspace, the model had
to be linearized for ease of simulation. This was accomplished using the
Jacobian Linearization method. The hand calculations shown below were completed
in partnership with the partner group of Rick Hall and Jacob Winkler.
@image html Lab6HC1.png width=600in
@image html Lab6HC2.png width=600in
@image html Lab6HC3.png width=600in
@image html Lab6HC4.png width=600in

@section page_Lab6_MATLABMet MATLAB Method
The response of the open loop system was simulated in MATLAB using the state space modeling
tools of the Controls System Toolbox. The "state space" variable is shown below:
@image html Lab6SSmat.png width=600in

The closed loop model was evaulated using a Simulink model as to visually represent
the feedback present in the system. The simulink block diagram is shown below:
@image html Lab6Simulink.png width=600in

@section page_Lab6_Case1 Open Loop Case 1 Results
Case 1 places the ball at the equilibrium point with no initial platform velocity or motor torque. 
The simulation results are shown below:
@image html Lab6Case1.png width=600in
Here, the model is behaving exactly as expected. Because the system is released
at equilibrium, there is no movement in the system over the course of the simulation.

@section page_Lab6_Case2 Open Loop Case 2 Results
Case 2 offsets the equilibrium point by 5cm with no initial platform velocity, tilt or motor torque.
The simulation results are shown below:
@image html Lab6Case2.png width=600in
There are two different regions in this graph. In the second region, past approximately
0.2s, the ball is behaving as expected. The weight of the ball causes the table to
tip over and the ball to roll away from the equilibrium point. Because this model
does not represent ANY of the physical limitations of the platform, we see the 
angular velocity and tilt of the system continue to increase as there are no
stops to prevent movement. There is an irregularity in the opening section of
the graph where the ball momentarily rolls towards the equilibrium point. The
causes of this irregularity are under investigation.

@section page_Lab6_Case3 Open Loop Case 3 Results
Case 3 places the ball at the equilibrium point with no initial velocity. The platform is tilted
by an initial five degrees, but has no angular velocity. The motor provides no torque.
The simulation results are shown below:
@image html Lab6Case3.png width=600in
The simulation results fully match the expected kinematics of the system in this case.
We see the position and velocity of the ball begin at zero and initially act as if there
is a constant acceleration. The graphs then transition to a less constant velocity as the
platform begins to accelerate angularly. We also see the Angular tilt of the platform 
gradually increases as the ball moves further from the equilibrium point. The system
almost acts as a "positive feedback system" getting further and further from equilibrium
as the simulation continues. This is exactly as expected as the ball would roll off the platform
in this situation with no control or motor torque.

@section page_Lab6_Case4 Open Loop Case 4 Results
Case 4 places the ball at the equilibrium point with no intial velocity. The platform
has no initial tilt or angular velocity. The motor provides an impulse torque of
1 mN*mm at time 0. The simulation results are shown below:
@image html Lab6Case4.png width=600in
The two clear sections of this graph also match the expectations of the physical
system. There is a clear linear portion in the Angular Velocity graph. This relates
directly to an applied torque from the motor. Then, the graphs switch to the 
unforced system response, but the initial conditions are no longer zero. At this
point we see the resposne as expected, very similar to case 2 where the ball 
continually rolls further from equilibrium forcing the platform to tilt further and 
the angular velocity of the plate to increase.

@section page_Lab6_ClosedCase Closed Loop Case Results
The closed loop case emulates the initial conditions of Open Loop Case 2. However,
the closed loop uses a precalculated feedback loop with a feedback gain as shown
below. The simulation results are also shown below.
@image html Kcontrol.png Copyright: Charlie Revfem width=600in

@image html Lab6ControlledCase.png width=600in
The controller acts as expected, overshooting and oscillating about the equilibrium
point until reaching it. This is expected with the given K value as we are instructed that
the K values are not optimized, but simply passable. The system oscillation is not optimal,
but the system does reach the desired steady state (equilibrium) for all system states.

@page page_Lab7 Lab 7: Resistive Touch Panel Driver

@section page_Lab7_desc Description
In this lab, it was desired to create a driver (\ref touchdriver.py) for the ER-TFT080-1 Resistive Touch Panel.
This driver was to include four methods: three methods to read individual components of the
panel and one method to read all three components and return them as a touple. Also included in
this lab file are a test file for calibrating the driver (\ref Lab7test.py) and testing the time response of the driver.

@section page_Lab7_src Source Code Access
You can access the source code for this exercise here: https://bitbucket.org/kquizon/me-405/src/master/Lab6/

@section page_Lab7_hardware Hardware Setup
Below are three views of the hardware setup used in this lab exercise.
@image html HardwareISO.jpg Full System width=600in
@image html Hardwareports.jpg Nucleo PCB Ports width=600in
@image html Hardwareover.jpg Overheard View of System width=600in

@section page_Lab7_time Time Response of Driver
Below is a graph of the time response of one thousand calls of the driver to read
all three channels. We see from thsi graph that the data is spread about 1500 us with
an average slightly above 1500 us. This was deemed sufficient in speed for this lab
exercise.
@image html TimeResponseGraph.png width=600in

@page page_Lab8 Lab 8: Term Project Part I (Motors and Encoders, oh my!)
@image html TPMotor.jpg DCX22S 12VDC Step Motor in Term Project Hardware Mount width=600in
@image html TPEncoder.jpg Encoder Attached to Output Shaft in Term Project Hardware width=600in

@section page_Lab8_desc Description
The code on this page documents the motor and encoder drivers used in the 
platform balancing system for the Term Project. The Encoder driver is
responsible for setting up the encoder on the user provided pins, initializing the encoder and timer
and reading the measured angle of the output shaft. The driver is written as a Python
class. The encoder driver is commonly used (like in the term project) with the 
E4T-1000-236-S-D-M-B encoder shown above.
The Motor Driver is responsible for setting up the neccesary pins and
controlling the power to drive each motor in the system using pulse width modulation. 
This driver is also contained in a Python class. While the motor driver is designed for use
with any standard two pin stepper motor with an enabling pin, the class is commonly employed in further
uses in this documentation with the DCX22S 12VDC Stepper Motor (pictured above). \n
\n
The \ref Encoder.py contains six methods. An initialization method that initializations 
the encoder with the user provided pins and timer. The PPR is default set at 4000, but can be changed
if the PPR is different for the desired encoder. The functional methods include:\n
1. update: update the encoder position by reading from the timer and calculating a good delta\n
2. get_position: return the encoder's current position in degrees\n
3. set_position: (set the encoder's current position to a desired value)\n
4. get_delta: calculate the change between the two most recent encoder readings, correcting for timer overflow \n
5. zero: set the current encoder position to zero (the home) position \n
\n
The \ref MotorDriver.py contains four methods. An initialization method that initializes the
motor requested with the user provided pins and timer. The functional methods include:\n
1. enable: enable the motor for motion\n
2. disable: prevent motor motion\n
3. set_duty set the motor duty cycle using the percent of active pulse time (PWM) \n
\n
These drivers were developed in conference with Anil Singh, Rick Hall, and Jacob Winkler, who's repositories may be navigated to from the home page. 

@section page_Lab8_src Source Code Access
Source Code related to Lab 8 is located at: https://bitbucket.org/kquizon/me-405/src/master/Lab8//

@section page_Lab8_doc Documentation
Documentation related to the Encoder Driver is located at \ref Encoder.py \n
Documentation related to the Motor Driver is located at \ref MotorDriver.py

@page page_TPH Term Project Hardware

@section page_TPH_Desc Description
This page outlines the hardware setup used in the term project. We will trace the command line
and sensor feedback of the entire system and establish the basic functionality of each component,
referencing it's corresponding driver.

@section page_TPH_Controller Controller Mounts
The system is controlled by a Nucleo L476RG Microcontroller. The Nucleo stores
the control files and hosts teh output csv file. The Nucleo is mounted on a 
custom PCB created by Charlie Refvem (CC BY-NC-SA 4.0). This PCB contains solder
points for all the GPIO pins of the nucleo, as well as screw terminals for the 
motor inputs and ports for the two encoders. The PCB is also capable of interfacing
with the DSD Tech HM19 Bluetooth chip, but this port is not utilized for this project.\n
\n
The board also has a USB Type B port to communicate with the Nucleo. This port hosts
a virtual flash drive that allows files to be "dragged and dropped" instead of imported
using the ampy module. Finally, the board features a port for connecting to a 12VDC power
source. This external power source allows the entire system to be run independently from
a computer, assuming that the file desired to be run is correctly installed on the Nucleo.\n
\n
The complete system with all applicable connections established is shown below.
@image html TPMCU.jpg Microcontroller Mount and PCB width=600in

@section page_TPH_Motor Actuators
@image html TPMotor.jpg DCX22S 12VDC Step Motor in Term Project Hardware Mount width=600in
The actuators in this system are two DCX22s 12VDC Stepper Motors (shown above,controlled by \ref MotorDriver.py). These stepper motors are
connected via a square toothed belt to a larger gear attached to an output shaft. The gear ratio
between the motor and the output shaft is measured to be approximately 4.3 (NOTE: The encoder is
attached to the output shaft.) The output shaft is then connected to a lever arm.
This lever is mounted via a plastic/metal ball joint to a push rod. 
This push rod has a second ball joint at its mounting location to the top plate. 
This is a rigid connection, the screw is not allowed to translate in the slot attached
to the top plate. This allows the motion of the motor to be translated into 
corresponding motion of the top plate. The motor lever has a length of 110mm
while the push rod is 60mm from joint to joint. This gives the conversion
from output shaft angle to top plate angle (60/110). 

@section page_TPH_Encoder Sensor 1: Output Shaft Encoder
@image html TPEncoder.jpg Encoder Attached to Output Shaft in Term Project Hardware width=600in
Attached to the output shaft is a E4T-1000-236-S-D-M-B encoder, shown above
in its mounted position. This is the closer sensor to the actuators and can be 
used to determine the angle of tilt of the platform. The encoders are driven by
the encoder driver class (\ref Encoder.py). This encoder is attached to the PCB
and provides real time readings to the Nucleo, as called by the software. The
output of the encoder is multiplied by the conversion factor determined by the geometry
of the system (60/110,described above) to determine the tilt angle of the top plate
in a specific dimension. The x encoder is related to the tilt about the y axis, and
the y encoder is related to the tilt about the x axis.
\n
These encoders may be used in liu of the BNO055 IMU if desired, but have several drawbacks.
The largest drawback comes with initialization. Upon startup, the encoders return a 
zero, regardless of their true position. This requires that the system be initialized
in the balanced position, otherwise there will exist a steady state error related
to the intiial tilt of the platform. Further, because the encoders are attached
to the output shaft, any slippage of the belts results in unreliable, meaningless
encoder readings. Generally, this shows the superiority of the IMU for collecting
top plate movement and angles.

@section page_TPH_IMU Sensor 2: BNO055 IMU
@image html TPIMU.jpg The BNO055 IMU Mounted to the Top Plate width=600in
The BNO055 IMU (shown above) has a sensor suite capable of measuring position in quaternions,
heading, gyroscopic rotation, linear velocity and acceleration, and the gravity vector. 
Using the gravity vector,the heading, and gyroscoping rotation, the angle of tilt
of the top plate and the angular velocity of the top plate may be determined.
Because these state variables are measured directly by the IMU, it is much more
reliable than the Encoder method, as described above. The BNO055 communicates
with the Nucleo via I2C protocols.

@section page_TPH_touch Sensor 3: Touch Panel
@image html TPOverhead.jpg Overhead Shot of Touch Panel Mounted to Top Plate width=600in
The third sensor utilized to record the position of the ball along the top plate is
an ER-TFT080-1 Resistive Touch Panel (shown above). The resistive touch panel
functions as a voltage divider with varying voltages depending on the location of the
ball in the x and y planes. This sensor is the slowest of those used in this project,
taking approximatley 1500 us to read all three dimensions as pins must be reassigned.
The RTP communicates with the Nucleo via four GPIO pins. Because the RTP's output
is a printed flexible circuitry strip, a converter chip is required between the
RTP and the Nucleo pins (shown below).

@section page_TPH_function Hardware Functionality
@image html TPISO.jpg Isometric View of Full System width=600in
From a top level perspective, the Nucleo takes reading from two sensors and commands
a duty cycle to the motors, resulting in a change in the angular position of the
top plate and linear position and velocity of the ball. The system functions using
EITHER the Encoders or the IMU, not both. 

@page page_TPS Term Project Software

@section page_TPS_desc Description
This page discusses the general layout and functionality of the software used
to drive the Term Project Hardware, who's main script is contained in \ref projectFile.py

@section page_TPS_src Source Code Access
The source code for the term project may be accessed here: https://bitbucket.org/asinghcp/me-405-repository/src/master/Term%20Project/

@section page_TPS_drivers Drivers
During operation, the term project code calls three drivers. However, because
the system may be run with either the encoders or the IMU, four drivers are described
below.\n
\n
\ref MotorDriver.py controls the motor action of the system. The driver allows
for the enabling and disabling of motors, as well as the feeding of a duty cycle
of PWM to drive the motors at a particular velocity in a particular direction.
More detailed information on the \ref MotorDriver.py may be viewed in \ref page_Lab8.\n
\n
\ref touchdriver.py interacts with the Resistive Touch Panel. It handles the creation
of and reassignment of pins in order to efficiently read the three dimensions of
the touch panel. This driver directly provides two of the state variables of the
system, x and y. When called with the timing of the task, the velocity of the ball
(xdot and ydot) may also be derived. All of these readings are in relation to the center of the touch panel
as calibrated during \ref page_Lab7 \n
\n
\ref Encoder.py handles the outputs of the Encoders attached to the output shaft.
The driver is capable of returning both the position of the encoder and the delta,
which when called with the timing of the task, may be used to find two more state variables:
the angle of tilt of the top plate and the angular velocity of the top plate. More
information on the encoder driver may be read in \ref page_Lab8 \n
\n
\ref bno055.py handles the outputs and calibration of the BNO055 IMU. This driver
was written by Radomir Dopieralski for Adafruit industry and is used under the
MIT License (MIT), found via the open source platform github. The document is unmodified, 
with the exception of the addition of doxystring comments for clarity. The driver
allows for the return of the heading and gyroscopic motion of the top plate,
directly providing four state variables: thetax, thetay, thetax_dot, and thetay_dot.

@section page_TPS_fileshare Task Queueing and Priority Organization
The main script for controlling the hardware uses a priority based cooperative
task execution scheme developed by Prof. John Ridgely and used under the GNU Copyright V3.0. This cooperative multitasking
is executed through two main files: \ref cotask.py and \ref task_share.py.\n
\n
\ref cotask.py allows for the creation of task objects with a user assigned priority and period. Then,
a priority scheduler is called on the list of task objects. The priority scheduler checks
which tasks are ready to be executed based on their period and then executes the ready
tasks in order of priority. In this lab, the necessary actions were divided into 6 tasks:\n
\n
1. X Axis Controller\n
2. Y Axis Controller\n
3. Touch Panel Data Collection\n
4. IMU Data Collection\n
5. Encoder Data Collection\n 
6. Write Data to CSV\n
\n

While there are six possible tasks, only five tasks are ever executed as the system
is run either on the IMU or the encoders. The tasks above are listed in the order
of assigned priority, with the controllers having the highest priority and write data
the lowest. A description of each task is below.\n
\n
X Axis Controller: The X Axis Controller takes data from each of the queues,
performs mathematic operations to convert units (degrees to rads) and then uses
the state space feedback model (T= K*X, where X is the state space matrix) to 
determine the necessary torque for the system correction. The necessary torque
is converted to a duty cycle using the motor characteristics. The duty cycle is
then fed to the motor. The X axis controller has the highest priority and is
executed with a period of 2.5ms.\n
\n
Y Axis Controller: The Y Axis Controller takes data from each of the queues,
performs mathematic operations to convert units (degrees to rads) and then uses
the state space feedback model (T= K*Y, where Y is the state space matrix) to 
determine the necessary torque for the system correction. The necessary torque
is converted to a duty cycle using the motor characteristics. The duty cycle is
then fed to the motor. The Y axis controller has the second highest priority and is
executed with a period of 2.5ms.\n
\n
Touch Panel Collection: Touch Panel collection calls the \ref touchdriver.py to 
collect data from the touch panel. It then process that data using time stamps
to produce linear velocity data for the ball. It then stores the position and velocity
of the ball in the appropriate queues. The Touch Panel Collection task has the highest
priority of the sensor tasks and is executed with a period of 2.5 ms.\n
\n
IMU Data Collection: IMU Data collection calls the \ref bno055.py driver and collects
heading and gyroscopic data from the IMU. The data needs to postprocessing and is
immediately stored into the appropriate queues for the angular position and velocity
of the top platform.The IMU Data Collection task is equal priority to the Encoder task
and the lowest of the sensor tasks. It is run with a period of 5ms. \n
\n
Encoder Data Collection: Encoder Data Collection calls the \ref Encoder.py driver
and collects the angular position of the output shaft from the encoder. This data
is processed to the angular position and acceleration of the platform using the geometry
of the system and a time stamp in the Encoder Data Collection task. The processed
data is then stored in the appropriate queue. The Encoder Data Collection task is 
of equal priority to the IMU Collection task and the lowest of the sensor tasks.
It is run with a period of 5ms. \n
\n
Write Data to CSV: If enabled, the Write Data to CSV task opens a csv file and 
writes the specified data (four of the eight state variables) to the csv file.
It is the lowest priority task called at the highest interval of 50ms.\n
\n
\ref task_share.py allows for the creation of Queue and Share objects such that data and booleans 
may be shared between various tasks in the task list. Queues function as arrays with
a moving pointer that keeps track of data present and what may be accessed. Shares
are single value variables that may be accessed by any task. The core functionality
of the \ref task_share.py is to handle the disabling and enabling of interrupts and other
functionality that may corrupt data if called during data accessing. For this project,
each state variable (as seen in \ref page_Lab5) is assigned a queue. There is only one
share variable (fault) which describes the state of the motor. If fault is 1, then
a fault has been detected and no controllers will be run. A task diagram of how
data is shared between the tasks called is shown below. Note that the encoder
and IMU tasks create the same data and as such only one is called when the code
is executed.
@image html TPTaskShare.png Variables Shared Between Tasks width=600in

@section page_TPS_Fault Fault Handling
To prevent damage to hardware or potential injury, the code is built with fault
detection and handling. Using the DCX22's built in nFAULT pin (which falls low if
an overcurrent condition is detected), an external interrupt was built. The external
interrupt stores data into a Shares variable and disables the motors. Processing power
is saved by disabling the controllers while the fault is live. The fault may be cleared
by the user in the REPL terminal when conditions are again clear.

@section page_TPS_Functionality General Functionality
A top level via of the code may descirbed by the following steps:\n
1. Initialize hardware, queues, share variables, and task objects\n
2. Initialize the priority scheduler and interrupt for faults\n
3. The priority scheduler runs each of the five tasks in order of priority and period:\n
4. Sensors collect data and store it in the appropriate queues\n
5. The controller runs with the updated data from the queues\n
6. Steps 4 and 5 are repeated ad infinity until user input or a fault is detected.

@section page_TPS_User User Manual
To successfully operate the tilt table using the software described above, a 
user must make four major decisions. These booleans, contained under the if name = main
portion of \ref projectFile.py control which aspects of the system are run. The first
two booleans enable or disable individual axis controllers. The third boolean allows
the user to choose between measuring table angle from the encoders or the BNO055 IMU.
The final boolean toggles data collection. After choosing which tasks to run,
the user simply loads and executes the file. No input is needed to start control.
It is recommend that the user wait for control to be initiated before placing the ball.
The user may then place the ball atop the platform and observe the system response.
If a motor overcurrent fault is detected, the user may restart the system when the
hardware is clear by typing "Fault Cleared" into the REPL command line.

@page page_TPR Term Project Results

@section page_TPR_desc Description
This page discusses the results of the completed code, including tuning and
final ball balancing.

@section page_TPR_tuning Tuning the Table
First, the table was tuned to balance itself without any input to the touch
panel. Initially, user inputs were used to disable one axis allowing the tuning
of a single axis at a time. This had advantages in simplicity, but disadvantages
as the assumption that the axes are uncoupled is not entirely true. Below, one
may view tuned results of disturbance tests to the x and y axes.
@image html XAxisDisturbanceTest.png width=600in
@image html YAxisDisturbanceTest.png width=600in
/n
We see from both of these tests that for the gains and nonlinear parameters selected,
the top plate exhibits a "dead zone" where it is not perfectly zero, but unable
to correct itself due to the dead zone in the motors. This had to be introduced 
as removing a dead zone prevented stability from being achieved, leading to jittery
behavior that disrupted the ball and lead to instability. As such, the above
tests were deemed acceptable./n
/n
A video of these disturbance tests with both axes enabled is at the link shown below. We see
that the table responds well without input from the touch panel and is very stable
for small angles, but does begin to oscillate slightly with larger angles (as expected
since our model utilized the small angle approximation).\n
\n
https://www.youtube.com/watch?v=0C4BL-Nb0OA
\n

@section page_TPR_balancing Balancing the Ball
Ultimately, due to both the variations in hardware, sensitivity to gain values,
and general unpredictability of the rolling ball, balancing the ball was only
achieved sporadically. However, some of these sporadic successes were captured
in various means. Below, the x and y response of a short, but successful test
are shown. The data markers represent the collection interval (approximately 50 ms),
while the interpolated line shows the rough movement of the ball. We see the expected
behavior in x, and some coupling in y. The x behavior is nearly a textbook second order
response. The system overshoots, then we see the ball oscillate slightly and settle
in the "dead zone" (described above). The y response is slightly different. Because
the two axes are coupled, we see the ball remains in the positive y for the entire
test, but still settles in the dead zone when the platform balances again.
@image html XResponseGraph.png width=600in
@image html YResponseGraph.png width=600in
The video in the link below shows the primary behavior of the fully functional system. The
system is largely stable for small angles of the platform and small velocities of
the ball. However, once the ball begins to move too quickly or the platform tilts
too far, the system is unable to respond, the ball is dropped. Once the ball drops
off the platform, the platform returns successfully to zero. All of this behavior
shows that the system is performing correctly, we even see the ball tend towards
zero in all instances. Repeatedly balancing the ball in various circumstances
simply requires further tuning the gains and nonlinear parameters of the system.
We call this MISSION SUCCESS!\n
\n
https://www.youtube.com/watch?v=7MbAT1GizgQ
\n
@section page_TPR_partner Partnership
This term project was completed in partnership with Anil Singh, who's documentation may be viewed here:
https://asinghcp.bitbucket.io \n
Considerable work was also done on the term project in conference with Rick Hall
and Jacob Winkler. The results displayed above are a joint effort between these four
engineers.
'''
    