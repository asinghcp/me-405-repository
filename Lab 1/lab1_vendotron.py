'''
@file lab1_vendotron.py
@brief file running a virtual vending machine "Vendotron"
@details This file contains a script that runs a virtual vending machine 
         called Vendotron. After being greeted by a startup message, the
         user is able to select a preferred beverage at any time. The user
         can also input coins or bills. The script will determine if the
         correct change has been provided, and return a drink, or
         an insufficient funds message. At any point the user can eject their
         balance. 
@author Anil Singh
@date January 21, 2021
'''
import keyboard

## Initializes Code 
state = 0
last_key = ''

def getChange(price, payment):
    '''
    @brief getChange function for homework assignment 1.
    @details This contains a function called getChange. getChange is designed
             to accept a payment and store the price for an item. When a
             payment is given, a tuple representing the change is returned. 
    @param price This is the price of the desired object. Price is an integer in cents.
    @param payment This is the payment, which indicates the given payment denominations as a tuple. 
    @return The required change is returned in the fewest number of denominations.
    '''
    ## Convert payment into pennies and references given tuple
    pennies  = 1*payment[0]
    nickels  = 5*payment[1]
    dimes    = 10*payment[2]
    quarters = 25*payment[3]
    ones     = 100*payment[4]
    fives    = 500*payment[5]
    tens     = 1000*payment[6]
    twenties = 2000*payment[7]
    
    ## Stores total payment value in pennies
    pay_amount = pennies + nickels + dimes + quarters + ones + fives + tens + twenties
    
    ## Inform user they have insufficient funds (Returns "None")
    change = pay_amount - price
    if change < 0:
        return None
    
    ## Compute change
    elif change >= 0:
        value = [2000, 1000, 500, 100, 25, 10, 5, 1]
        
        ## Create empty list for storing list of tuple values
        change_list = []      
        for n in value:
            ## Algebra for connversions and remainders
            if change - n >= 0:
                x = change - change%n
                y = x/n
                change_list.append(int(y))
                change = change - x
            elif change - n < 0:
                change_list.append(0)
            else:
                pass
        change_list.reverse()
        change_value = pay_amount - price
        return change_list, change_value
        
def printWelcome():
    '''
    @brief Prints Welcome Message and Beverage Prices
    @details The welcome message displays a greeting, drink prices, and
             current funds. 
    '''
    print('''
-------------------------------------------------------------          
|          Welcome to Vendotron!                            |
|          Please insert funds and select a drink:          |
|          Cuke           $1.00 (c)                         |
|          Popsi          $1.20 (p)                         |
|          Spryte         $0.85 (s)                         |
|          Dr. Pupper     $1.10 (d)                         |
|                                                           |
|          Current Funds = $0.00                            |
|                                                           |
|          To insert funds please use the following keys:   |
|          Pennies:  (0)                                    |
|          Nickles:  (1)                                    |
|          Dimes:    (2)                                    |
|          Quarters: (3)                                    |
|          Ones:     (4)                                    |
|          Fives:    (5)                                    |
|          Tens:     (6)                                    |
|          Twenties: (7)                                    |          
-------------------------------------------------------------       
          ''')
    pass

def kb_cb(key):
    '''
    @brief Callback Function for when Key is Pressed
    @param key Refers to the key on the keyboard that has been pressed
    '''    
    global last_key
    last_key = key.name
    
## Forces keyboard module to respond to specific keys    
#keyboard.on_release(callback=kb_cb)
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)

keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("6", callback=kb_cb)
keyboard.on_release_key("7", callback=kb_cb)

while True:
    '''
    Implements FSM
    '''
    ## Runs state 0 Code - Initialization State
    if state == 0:
        printWelcome()
        Funds = [0,0,0,0,0,0,0,0]
        state = 1
        
    ## Runs state 1 Code - Holding State
    elif state == 1:
  
        ## For fund insertion, transition to state 2
        if last_key == '0':
            last_key = ''
            Funds[0] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '1':
            last_key = ''
            Funds[1] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '2':
            last_key = ''
            Funds[2] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '3':
            last_key = ''
            Funds[3] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '4':
            last_key = ''
            Funds[4] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '5':
            last_key = ''
            Funds[5] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '6':
            last_key = ''
            Funds[6] += 1
            state = 2 ## Transition from s1 to s2
        elif last_key == '7':
            last_key = ''
            Funds[7] += 1
            state = 2 ## Transition from s1 to s2
        
        ## For drink selection, transition to state 3
        elif last_key == 'c':
            last_key = ''
            drink = 'Cuke'
            ## An integer number of cents representing the price of the desired drink
            price = 100 ## price of cuke in cents
            state = 3 ## Transition from s1 to s3
        elif last_key == 'p':
            last_key = ''
            drink = 'Popsi'
            price = 120 ## price of popsi in cents
            state = 3 ## Transition from s1 to s3
        elif last_key == 's':
            last_key = ''
            drink = 'Spryte'
            price = 85 ## price of spryte in cents
            state = 3 ## Transition from s1 to s3
        elif last_key == 'd':
            last_key = ''
            drink = 'Dr. Pupper'
            price = 110 ## price of dr. pupper in cents
            state = 3 ## Transition from s1 to s3
            
        ## For ejection, transition to state 4    
        elif last_key == 'e':
            last_key = ''           
            ## Variable that uses getChange funtion to compute change.
            ejected_change = getChange(0, Funds) 
            Funds = ejected_change[0]
            state = 4 ## Transition from s1 to s4
            
        ## Error Handling    
        else:
            pass
        
    ## Runs state 2 Code - Addition State
    elif state == 2:
        
        curr_Fund = (1*Funds[0] + 5*Funds[1] + 10*Funds[2] + 25*Funds[3] + 100*Funds[4] + 500*Funds[5] + 1000*Funds[6] + 2000*Funds[7])/100
        print('''-------------------------------------------------------------
|                                                           |''')
        print('|          Current Funds = $' + '{:.2f}'.format(curr_Fund) + '                            |')
        state = 1 ## Transition back to Holding State
        print('''|          To insert more funds please use the following    |    
|          keys:                                            |
|                                                           |
|          Pennies:  (0)                                    |
|          Nickles:  (1)                                    |
|          Dimes:    (2)                                    |
|          Quarters: (3)                                    |
|          Ones:     (4)                                    |
|          Fives:    (5)                                    |
|          Tens:     (6)                                    |
|          Twenties: (7)                                    |  
                            To eject balance, press (e)     |
-------------------------------------------------------------''')        
    ## Runs state 3 Code - Change State & Dispense State
    elif state == 3:
        
        ## Uses getChange function to calculate change
        change = getChange(price, Funds)
        if change == None:
            print('Not Enough Funds! ' + drink + ' costs $' + '{:.2f}'.format(price/100) + '. Insert more funds.')
            drink = ''
            price = None
            state = 1 ## Transition from s3 to s1
            
        elif change[1] >= 0:
            print('Vending ' + drink + '...')
            print('''
      ░░▒▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒██▓▓▒▒▒▒████▓▓▒▒▒▒▒▒░░    
      ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒    
    ▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒  
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ░░    ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ░░          ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ░░                ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ░░                  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ░░                      ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
  ░░                          ▒▒▒▒▒▒▒▒▒▒            
  ██▓▓▒▒▒▒▒▒▒▒▒▒                                    
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒                                
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒            ▒▒▒▒▒▒    
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ████▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ████▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██████▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
  ██████████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    ██████████████████████████████████████████████  
        ██████████████████████████████████████      
          ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒        
                  ''')
            drink = ''
            price = None
            Funds = change[0]
            state = 4 ## Transition from s3 to s4
            
        ## Error Handling    
        else: 
            pass
        
    ## Runs state 4 Code - Eject Change State
    elif state == 4:

        print('Please Take your change:' )
        print(str(Funds[0]) + ' pennies, ' + str(Funds[1]) + ' nickles, ' + str(Funds[2]) + ' dimes, ' + str(Funds[3]) + ' quarters, ' + str(Funds[4]) + ' ones, ' + str(Funds[5]) + ' fives, ' + str(Funds[6]) + ' tens, and ' + str(Funds[7]) + ' twenties ')
        print('Thank you for Using Vendotron!')
        Funds = [0, 0, 0, 0, 0, 0, 0, 0] 
        change = None
        printWelcome()
        state = 1 # s4 -> s1

    ## Error Handling
    else:
        pass    

